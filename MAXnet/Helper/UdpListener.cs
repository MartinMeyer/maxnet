using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace MAXnet
{
    public class UdpListener
    {
        private readonly int listenPort;

        public UdpListener(int listenPort = 23272)
        {
            this.listenPort = listenPort;
        }

        public void StartAsync()
        {
            UdpClient listener = new UdpClient(this.listenPort);
            IPEndPoint groupEp = new IPEndPoint(IPAddress.Any, this.listenPort);

            try
            {
                while(true)
                {
                    Console.WriteLine("Waiting for broadcast");
                    byte[] bytes = listener.Receive(ref groupEp);

                    Console.WriteLine($"Received broadcast from {groupEp} :");
                    Console.WriteLine($" {Encoding.ASCII.GetString(bytes, 0, bytes.Length)}");
                }
            }
            catch (SocketException e)
            {
                Console.WriteLine(e);
            }
            finally
            {
                listener.Close();
            }
        }
    }
}