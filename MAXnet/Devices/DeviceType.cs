namespace MAXnet.Devices
{
    public enum DeviceType
    {
        Cube = 0,
        RadiatorThermostat = 1,
        RadiatorThermostatPlus = 2,
        WallThermostat = 3,
        ShutterContact = 4,
        EcoButton = 5,
    }
}