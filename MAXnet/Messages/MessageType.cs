namespace MAXnet.Messages
{
    public enum MessageType
    {
        Hello = 0,
        NtpServer,
        DeviceList,
    }
}